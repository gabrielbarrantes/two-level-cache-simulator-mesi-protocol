# Two Level Cache Simulator-MESI Protocol

A simulator of a cache two level cache, using the MESI protocol for consistency.
The level two cache is a direct mapped 128KB memory and the level one is a 16KB,
two way-set associative, LRU. The simulation uses only two caches of level one. 

The program takes a input file with the list of memory addresses, and simulates 
the behavior of the three caches keeping track of the misses, hits and 
invalidations for consistency. The accesses mod 4 = 0 are from processor cero, 
and the rest are from processor one.

## Example of input file:

        LS Address  IC

        # 0 7fffed80 2
        # 0 10010000 5
        # 0 10010060 3
        # 0 10010030 10
        # 0 10010004 4
        # 0 10010064 6
        # 0 10010034 7

## Mode of use:

$ gunzip -c <file.gz> | cache

### Example of use:
    $ gunzip -c mcf.trace.gz | ./cache

### Output:
    Simulation results Level 1 Cache #0:
    Overall miss rate 0.765816
    Read miss rate    0.758854
    Load  misses      1059956
    Store misses      269474
    Total misses      1329430
    Load  hits        336829
    Store hits        69706
    Total hits        406535
    
    Simulation results Level 1 Cache #1:
    Overall miss rate 0.523695
    Read miss rate    0.483817
    Load  misses      2028493
    Store misses      698854
    Total misses      2727347
    Load  hits        2164194
    Store hits        316351
    Total hits        2480545
    
    Simulation results Level 2:
    Overall miss rate 0.718841
    Read miss rate    0.662837
    Load  misses      2047908
    Store misses      869269
    Total misses      2917177
    Load  hits        1041702
    Store hits        99288
    Total hits        1140990
    
    Simulation results:
    Miss rate Global de L2              0.420109
    Miss rate L1 CPU0                   0.765816
    Miss rate L1 CPU1                   0.523695
    Invalidaciones por coherencia CPU0  255228
    Invalidaciones por coherencia CPU1  124212
    Time Elapsed:                        3.347034 seconds

