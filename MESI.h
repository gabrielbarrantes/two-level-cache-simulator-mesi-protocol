/*
 * MESI.h
 *
 *  Created on: Nov 8, 2018
 *      Author: gab
 */

#ifndef MESI_HPP_
#define MESI_HPP_

#include "MESIBlock.h"
#include "cacheLRU.h"
#include "cacheDirectMap.h"

class MESI
{
    public:
        MESI(CacheLRU * i_pCacheP1L1, CacheLRU * i_pCacheP2L1, CacheDirectMap * i_pCacheCommonL2);
        ~MESI();
    protected:
    private:    
        MESIBlock *m_pMESI;
};

#endif /* MESI_HPP_ */
