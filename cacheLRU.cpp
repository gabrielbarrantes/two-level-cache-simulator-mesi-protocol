/*
 * cacheLRU.cpp
 *
 *  Created on: Nov 8, 2018
 *      Author: gab
 */
 
#include <stdio.h>
#include <stdlib.h>

#include <math.h>
#include "cacheLRU.h"

#define wordSizeBytes 1
#define addressSizeBits 32

CacheLRU::CacheLRU(int i_iSizeKB, int i_iBlockSizeBytes, int i_iAssociativity, int i_iNumberOfCache, CacheDirectMap * i_pCacheCommonL2)
{
    m_pCacheCommonL2 = i_pCacheCommonL2;
    m_iNumberOfCache = i_iNumberOfCache;
    
    m_iSize = i_iSizeKB;
    m_iAssociativity  = i_iAssociativity;
    m_iBlockSizeBytes = i_iBlockSizeBytes;
    
    int l_iCacheSizeBytes = 16384;
    
    m_iNumberOfSets = l_iCacheSizeBytes/(i_iBlockSizeBytes*i_iAssociativity);
    m_iNumberOfVias = i_iAssociativity;
    
    m_iNumberOfIndexBits  = (int) (log(m_iNumberOfSets)/log(2));
    m_iNumberOfOffsetBits = (int) (log(i_iBlockSizeBytes/wordSizeBytes)/log(2));
    m_iNumberOfTagBits    = addressSizeBits - m_iNumberOfIndexBits - m_iNumberOfOffsetBits;
    
    //Generate masks
    m_uiIndexMask = 0;
    for(int i=0; i<m_iNumberOfIndexBits;i++){m_uiIndexMask = (m_uiIndexMask<<1)+1;}
    
    m_uiTagMask = 0;
    for(int i=0; i<m_iNumberOfTagBits;i++){m_uiTagMask = (m_uiTagMask<<1)+1;}

    //Set initial values
    m_dOverallMissrate  = 0.0;
    m_dReadMissRate     = 0.0;
    m_uiDirtyEvictions  = 0;
    m_uiLoadMisses      = 0;
    m_uiStoreMisses     = 0;
    m_uiTotalMisses     = 0;
    m_uiLoadHits        = 0;
    m_uiStoreHits       = 0;
    m_uiTotalHits       = 0;
    m_uiTotalHits       = 0;
    
    //Allocate memory for cache's blocks
    m_pCache = (cacheBlockLRUMESI **)malloc (m_iNumberOfSets*sizeof(cacheBlockLRUMESI *));
    //if (cache  == NULL){ return 1; }
    for (int i=0;i<m_iNumberOfSets;i++){m_pCache[i] = (cacheBlockLRUMESI *) malloc (m_iNumberOfVias*sizeof(cacheBlockLRUMESI));}
    //Set the initial values of cache
    for(int i=0;i<m_iNumberOfSets;i++)
    {
        for(int j=0;j<m_iNumberOfVias;j++)
        {
            //m_pCache[i][j].m_iValidBit  = 0; 
            //m_pCache[i][j].m_iDirtyBit  = 0;
            m_pCache[i][j].m_iLRUIndex  = 0;
            m_pCache[i][j].m_iMESIState = NULL;
        }
    }
    
}

void CacheLRU::DataAccess(long i_liAddress, int i_iLoadStore)
{
    unsigned int index = (i_liAddress>> m_iNumberOfOffsetBits) & m_uiIndexMask;
    unsigned int l_uiTag   = (i_liAddress>> (m_iNumberOfIndexBits+m_iNumberOfOffsetBits) ) & m_uiTagMask;
    
    int hit =0;
    int via =0;
    
    //Look for Hit
    for(int j=0; j< m_iNumberOfVias; j++)
    {
        if((m_pCache[index][j].m_uiTag == l_uiTag) && m_pCache[index][j].m_iMESIState != NULL )
        { 
            if( (*(m_pCache[index][j].m_iMESIState) != 0))
            {
                hit = 1;
                via = j;
                m_pCache[index][j].m_iLRUIndex=0;
                if(j==0){m_pCache[index][1].m_iLRUIndex++;}
                else{m_pCache[index][0].m_iLRUIndex++;}
            
                if(i_iLoadStore){ m_uiStoreHits++; }//m_pCache[index][j].m_iDirtyBit =1; }
                else { m_uiLoadHits++; }
            
                break;
            }
        }
    }
    
    //if not hit, look envit candidate and replace the block in cache
    if(hit==0)
    {        
        int envitCandidate=0;
        
        for(int j=0; j< m_iNumberOfVias; j++)
        {
            if(m_pCache[index][j].m_iMESIState == NULL)
            {
                envitCandidate=j; break;
            }
            if((* (m_pCache[index][j].m_iMESIState) == 0)){envitCandidate=j; break;}
            
            if(m_pCache[index][j].m_iLRUIndex > m_pCache[index][envitCandidate].m_iLRUIndex )
            { 
                envitCandidate=j;
            }
        }
        
        m_pCache[index][envitCandidate].m_iLRUIndex = 0;
        if(envitCandidate==0){m_pCache[index][1].m_iLRUIndex++;}
        else{m_pCache[index][0].m_iLRUIndex++;}
        
        m_pCache[index][envitCandidate].m_uiTag = l_uiTag;
        
        if(i_iLoadStore)
        {
            m_uiStoreMisses ++; 
        } else {
            m_uiLoadMisses ++;
        }
        //* (m_pCache[index][envitCandidate].m_iMESIState) = 0;
        //* (m_pCache[index][envitCandidate].m_iMESIState) = 0;////////*********
        via = envitCandidate;
    }
    
    //printf("Tag via 0      %u\n" , m_pCache[index][0].m_uiTag);
    //printf("Tag via 1      %u\n" , m_pCache[index][1].m_uiTag);
    
    //llamar a la direccion en L2
    m_pCacheCommonL2->DataAccess(i_liAddress, i_iLoadStore, m_iNumberOfCache, & m_pCache[index][via], hit );

}

void CacheLRU::CalculateStatistics()
{
    m_uiTotalMisses=m_uiStoreMisses+m_uiLoadMisses;
    m_uiTotalHits=m_uiStoreHits+m_uiLoadHits;
    m_dReadMissRate= ((double ) (m_uiLoadMisses) )/ ((double) (m_uiLoadMisses+m_uiLoadHits) );
    m_dOverallMissrate= ((double) (m_uiTotalMisses) )/((double) (m_uiTotalMisses+m_uiTotalHits));
}

CacheLRU::~CacheLRU()
{
    //Allocate memory for cache's blocks
    //if (cache  == NULL){ return 1; }
    for (int i=0;i<m_iNumberOfSets;i++){ free(m_pCache[i]); m_pCache[i] = NULL; }
    free(m_pCache); m_pCache=NULL;
}
