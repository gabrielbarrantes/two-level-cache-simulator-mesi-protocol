/*
 * MESI.cpp
 *
 *  Created on: Nov 8, 2018
 *      Author: gab
 */

#include "MESI.h"

//MESI::MESI(int i_iNumerOfSuperiorBlocksPerCache, int i_iNumberOfSuperiorCaches)
MESI::MESI(CacheLRU * i_pCacheP1L1, CacheLRU * i_pCacheP2L1, CacheDirectMap * i_pCacheCommonL2)
{
    //Allocate memory for cache's blocks
    m_pMESI = (MESIBlock *)malloc (4096*sizeof(MESIBlock));
    
    for(int i=0;i<4096;i++)
    {
        m_pMESI[i]=  MESIBlock(i_pCacheP1L1, i_pCacheP2L1, &(i_pCacheCommonL2->m_pCache[i][0])); 
    }
    
}

MESI::~MESI()
{
    //Allocate memory for cache's blocks
    //if (cache  == NULL){ return 1; }
    //for (int i=0;i<m_iNumberOfSets;i++){ free(m_pCache[i]); m_pCache[i] = NULL; }
    //free(m_pCache); m_pCache=NULL;
}
