#include <stdio.h>
#include <stdlib.h>
#include "cacheLRU.h"
#include "cacheDirectMap.h"
#include <string.h>
#include <netinet/in.h>
#include <time.h>

#define DEBUG 0

int g_iAssociativity;         // Associativity of cache
int g_iBlocksize_bytes;       // Cache Block size in bytes
int g_iCachesize_kb;          // Cache size in KB

//Prints the usage of the program
void print_usage ()
{
    printf ("Usage: gunzip -c <tracefile> | ./cache");
    printf ("  tracefile : The memory trace file\n");
    exit (0);
}

int main(int argc, char * argv []) {
    clock_t tic = clock();
    // Process the command line arguments
    int j = 1;
    if (argc !=1){print_usage ();}
    
    //Create the caches
    CacheDirectMap * cacheCommonL2 = new CacheDirectMap(128, 32);    //Level2
    CacheLRU * cacheP0L1 = new CacheLRU(16, 32, 2, 0, cacheCommonL2);//Level1
    CacheLRU * cacheP1L1 = new CacheLRU(16, 32, 2, 1, cacheCommonL2);//Level1
    
    // Variables for store the read address and load/Store operation//
    int  l_iLoadStore;
    int  l_iICount;
    char l_cMarker;
    long l_lAddress;
    unsigned long int l_counter = 0; //counter for trace the number of processor of the address

    //Process address by address
    while (scanf("%c %d %lx %d\n",&l_cMarker,&l_iLoadStore,&l_lAddress,&l_iICount) != EOF) 
    {        

        if(l_counter % 4 ==0){ cacheP0L1->DataAccess(l_lAddress, l_iLoadStore); }// To P0
        else                 { cacheP1L1->DataAccess(l_lAddress, l_iLoadStore); }// To P1
        l_counter++;
        
        //////////////////////////
        //////////////////////////
        #if DEBUG  // Debug prints
        if(l_counter==0){
        printf("\033[1;36m""                            Cache0                Cache1                              CacheL2  \n""\x1b[0m"); //printf("Cache L2 \n");
        printf("\033[1;32m""                        Via0      Via1         Via0     Via1                           Tag       Valid    State0  State1  \n""\x1b[0m");
        
        }
        unsigned int indexL1 = (l_lAddress>> 5) & 255;
        unsigned int l_uiTagL1   = (l_lAddress>> (13) ) & 524287;
        
        unsigned int indexL2 = (l_lAddress>> 5) & 4095;
        unsigned int l_uiTagL2   = (l_lAddress>> (17) ) & 32767;
        
        if(l_counter % 2 ==0){ printf("\033[1;31m""P0   ""\x1b[0m"); }
        else                 { printf("\033[1;31m""P1   ""\x1b[0m");  }
        printf("\033[1;31m""IndexL1  %4u     TagL1 %4u    IndexL2  %4u    TagL2 %4u \n\n""\x1b[0m", indexL1, l_uiTagL1, indexL2, l_uiTagL2);
        printf("\033[1;33m""Tag/State in Set 0 |  %4d %c  |  %4d %c   |    %4d %c |  %4d %c  ""\x1b[0m", 
                cacheP0L1->m_pCache[0][0].m_uiTag, 
                cacheP0L1->m_pCache[0][0].m_iMESIState==NULL ? 'N' : *(cacheP0L1->m_pCache[0][0].m_iMESIState)==0 ? 'I' : *(cacheP0L1->m_pCache[0][0].m_iMESIState)==1 ? 'E' : *(cacheP0L1->m_pCache[0][0].m_iMESIState)==2 ? 'S' : 'M' , 
                cacheP0L1->m_pCache[0][1].m_uiTag,
                cacheP0L1->m_pCache[0][1].m_iMESIState==NULL ? 'N' : *(cacheP0L1->m_pCache[0][1].m_iMESIState)==0 ? 'I' : *(cacheP0L1->m_pCache[0][1].m_iMESIState)==1 ? 'E' : *(cacheP0L1->m_pCache[0][1].m_iMESIState)==2 ? 'S' : 'M',
                cacheP1L1->m_pCache[0][0].m_uiTag,
                cacheP1L1->m_pCache[0][0].m_iMESIState==NULL ? 'N' : *(cacheP1L1->m_pCache[0][0].m_iMESIState)==0 ? 'I' : *(cacheP1L1->m_pCache[0][0].m_iMESIState)==1 ? 'E' : *(cacheP1L1->m_pCache[0][0].m_iMESIState)==2 ? 'S' : 'M',
                cacheP1L1->m_pCache[0][1].m_uiTag,
                cacheP1L1->m_pCache[0][1].m_iMESIState==NULL ? 'N' : *(cacheP1L1->m_pCache[0][1].m_iMESIState)==0 ? 'I' : *(cacheP1L1->m_pCache[0][1].m_iMESIState)==1 ? 'E' : *(cacheP1L1->m_pCache[0][1].m_iMESIState)==2 ? 'S' : 'M'
                );
        printf("\033[1;37m"" Tag/State Set   0      %4d          %c       %c      %c \n""\x1b[0m", cacheCommonL2->m_pCache[0].m_uiTag , cacheCommonL2->m_pCache[0].m_iValidBit ? 'V' : 'I',                 cacheCommonL2->m_pCache[0].m_iMESIStateL1P0==0 ? 'I' : cacheCommonL2->m_pCache[0].m_iMESIStateL1P0==1 ? 'E' : cacheCommonL2->m_pCache[0].m_iMESIStateL1P0==2 ? 'S' : 'M' ,     cacheCommonL2->m_pCache[0].m_iMESIStateL1P1==0 ? 'I' : cacheCommonL2->m_pCache[0].m_iMESIStateL1P1==1 ? 'E' : cacheCommonL2->m_pCache[0].m_iMESIStateL1P1==2 ? 'S' : 'M');        
                
                
                
        printf("\033[1;33m""Tag/State in Set 1 |  %4d %c  |  %4d %c   |    %4d %c |  %4d %c  ""\x1b[0m", 
                cacheP0L1->m_pCache[1][0].m_uiTag, 
                cacheP0L1->m_pCache[1][0].m_iMESIState==NULL ? 'N' : *(cacheP0L1->m_pCache[1][0].m_iMESIState)==0 ? 'I' : *(cacheP0L1->m_pCache[1][0].m_iMESIState)==1 ? 'E' : *(cacheP0L1->m_pCache[1][0].m_iMESIState)==2 ? 'S' : 'M' , 
                cacheP0L1->m_pCache[1][1].m_uiTag,
                cacheP0L1->m_pCache[1][1].m_iMESIState==NULL ? 'N' : *(cacheP0L1->m_pCache[1][1].m_iMESIState)==0 ? 'I' : *(cacheP0L1->m_pCache[1][1].m_iMESIState)==1 ? 'E' : *(cacheP0L1->m_pCache[1][1].m_iMESIState)==2 ? 'S' : 'M',
                cacheP1L1->m_pCache[1][0].m_uiTag,
                cacheP1L1->m_pCache[1][0].m_iMESIState==NULL ? 'N' : *(cacheP1L1->m_pCache[1][0].m_iMESIState)==0 ? 'I' : *(cacheP1L1->m_pCache[1][0].m_iMESIState)==1 ? 'E' : *(cacheP1L1->m_pCache[1][0].m_iMESIState)==2 ? 'S' : 'M',
                cacheP1L1->m_pCache[1][1].m_uiTag,
                cacheP1L1->m_pCache[1][1].m_iMESIState==NULL ? 'N' : *(cacheP1L1->m_pCache[1][1].m_iMESIState)==0 ? 'I' : *(cacheP1L1->m_pCache[1][1].m_iMESIState)==1 ? 'E' : *(cacheP1L1->m_pCache[1][1].m_iMESIState)==2 ? 'S' : 'M'
                );
        printf("\033[1;37m"" Tag/State Set   1      %4d          %c       %c      %c \n""\x1b[0m", cacheCommonL2->m_pCache[1].m_uiTag , cacheCommonL2->m_pCache[1].m_iValidBit ? 'V' : 'I',                 cacheCommonL2->m_pCache[1].m_iMESIStateL1P0==0 ? 'I' : cacheCommonL2->m_pCache[1].m_iMESIStateL1P0==1 ? 'E' : cacheCommonL2->m_pCache[1].m_iMESIStateL1P0==2 ? 'S' : 'M',     cacheCommonL2->m_pCache[1].m_iMESIStateL1P1==0 ? 'I' : cacheCommonL2->m_pCache[1].m_iMESIStateL1P1==1 ? 'E' : cacheCommonL2->m_pCache[1].m_iMESIStateL1P1==2 ? 'S' : 'M');
        
        printf("\n");
        #endif
        ////////////////////////////
        ////////////////////////////
    } 
    ////////////////////////////////////////////////////////////////////
    //                                                                //
    //                        Print Results                           //
    //                                                                //
    ////////////////////////////////////////////////////////////////////

    //Print out results
    cacheP0L1->CalculateStatistics(); //calculate results
    printf("\n");
    printf("Simulation results Level 1 Cache #0:\n");

    printf("Overall miss rate %lf\n", cacheP0L1->m_dOverallMissrate);
    printf("Read miss rate    %lf\n", cacheP0L1->m_dReadMissRate);
    printf("Load  misses      %u\n" , cacheP0L1->m_uiLoadMisses);
    printf("Store misses      %u\n" , cacheP0L1->m_uiStoreMisses);
    printf("Total misses      %u\n" , cacheP0L1->m_uiTotalMisses);
    printf("Load  hits        %u\n" , cacheP0L1->m_uiLoadHits);
    printf("Store hits        %u\n" , cacheP0L1->m_uiStoreHits);
    printf("Total hits        %u\n" , cacheP0L1->m_uiTotalHits);
    
    //Print out results
    cacheP1L1->CalculateStatistics(); //calculate results
    printf("\n");
    printf("Simulation results Level 1 Cache #1:\n");

    printf("Overall miss rate %lf\n", cacheP1L1->m_dOverallMissrate);
    printf("Read miss rate    %lf\n", cacheP1L1->m_dReadMissRate);
    printf("Load  misses      %u\n" , cacheP1L1->m_uiLoadMisses);
    printf("Store misses      %u\n" , cacheP1L1->m_uiStoreMisses);
    printf("Total misses      %u\n" , cacheP1L1->m_uiTotalMisses);
    printf("Load  hits        %u\n" , cacheP1L1->m_uiLoadHits);
    printf("Store hits        %u\n" , cacheP1L1->m_uiStoreHits);
    printf("Total hits        %u\n" , cacheP1L1->m_uiTotalHits);
    
    //Print out results
    cacheCommonL2->CalculateStatistics(); //calculate results
    printf("\n");
    printf("Simulation results Level 2:\n");

    printf("Overall miss rate %lf\n", cacheCommonL2->m_dOverallMissrate);
    printf("Read miss rate    %lf\n", cacheCommonL2->m_dReadMissRate);
    printf("Load  misses      %u\n" , cacheCommonL2->m_uiLoadMisses);
    printf("Store misses      %u\n" , cacheCommonL2->m_uiStoreMisses);
    printf("Total misses      %u\n" , cacheCommonL2->m_uiTotalMisses);
    printf("Load  hits        %u\n" , cacheCommonL2->m_uiLoadHits);
    printf("Store hits        %u\n" , cacheCommonL2->m_uiStoreHits);
    printf("Total hits        %u\n" , cacheCommonL2->m_uiTotalHits);
    
    printf("\n");
    printf("Simulation results:\n");
    printf("Miss rate Global de L2              %lf\n", cacheCommonL2->m_dGlobalMissrate);
    printf("Miss rate L1 CPU0                   %lf\n", cacheP0L1->m_dOverallMissrate);
    printf("Miss rate L1 CPU1                   %lf\n", cacheP1L1->m_dOverallMissrate);
    printf("Invalidaciones por coherencia CPU0  %u\n" , cacheCommonL2->m_iCoherenceInvalidationsP0);
    printf("Invalidaciones por coherencia CPU1  %u\n" , cacheCommonL2->m_iCoherenceInvalidationsP1);

    clock_t toc = clock();
    printf("Time Elapsed:                        %f seconds\n", (double)(toc - tic) / CLOCKS_PER_SEC);

    return 0;
}
