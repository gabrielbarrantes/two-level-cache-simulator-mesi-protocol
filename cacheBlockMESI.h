#ifndef CACHEBLOCKMESI_H_
#define CACHEBLOCKMESI_H_

#include "cacheBlockLRUMESI.h" 
//Struct for representing a cache block

typedef struct 
{
    int m_iValidBit;
    int m_iDirtyBit;
    unsigned int m_uiTag;
    //int m_iLRUIndex;
    int m_iMESIStateL1P0; //0= invalid/ 1 = Exclusive / 2 = Shared / 3 = Modified
    //cacheBlockLRUMESI * m_pCacheBlockLRUMESIL1P0;
    int m_iMESIStateL1P1;
    //cacheBlockLRUMESI * m_pCacheBlockLRUMESIL1P1;

} cacheBlockMESI ;

#endif /* CACHEBLOCK_H_ */
