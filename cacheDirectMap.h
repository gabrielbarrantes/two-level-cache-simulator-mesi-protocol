/*
 * cacheDirectMap.h
 *
 *  Created on: Nov 8, 2018
 *      Author: gab
 */

#ifndef CACHEDIRECTMAP_HPP_
#define CACHEDIRECTMAP_HPP_

#include "cacheBlockMESI.h"
#include "cacheBlockLRUMESI.h"

class CacheDirectMap
{
    public:
        CacheDirectMap(int sizeKB, int blockSizeBytes);
        ~CacheDirectMap();
        
        void DataAccess(long i_liAddress, int i_iLoadStore, int i_iCacheNumber, cacheBlockLRUMESI * i_pCacheBlockLRUMESIL1, int i_iHitL1);
        void CalculateStatistics();
        
        int m_iSize;
        int m_iAssociativity;
        int m_iBlockSizeBytes;
        
        int m_iCoherenceInvalidationsP0;
        int m_iCoherenceInvalidationsP1;
        double       m_dGlobalMissrate;
        double       m_dOverallMissrate;
        double       m_dReadMissRate;
        unsigned int m_uiDirtyEvictions;
        unsigned int m_uiLoadMisses;
        unsigned int m_uiStoreMisses;
        unsigned int m_uiTotalMisses;
        unsigned int m_uiLoadHits;
        unsigned int m_uiStoreHits;
        unsigned int m_uiTotalHits;
        cacheBlockMESI *m_pCache;
        
    protected:
    private:
        int m_iNumberOfAccesses;
        int m_iNumberOfOffsetBits;
        int m_iNumberOfIndexBits;
        int m_iNumberOfTagBits;
        //int m_iRereferenceIntervalLength;
        int m_iNumberOfSets;
        int m_iNumberOfVias;
        unsigned int m_uiIndexMask;
        unsigned int m_uiTagMask;
        //cacheBlockMESI **m_pCache;
};

#endif /* CACHEDIRECTMAP_HPP_ */
