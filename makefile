cache: main.cpp cacheBlock.h cacheLRU.h cacheLRU.cpp cacheBlockLRUMESI.h cacheBlockMESI.h cacheDirectMap.h cacheDirectMap.cpp
	@echo "Compiling..."
	@g++ -o "cache" "main.cpp" "cacheLRU.cpp" "cacheDirectMap.cpp" -lm
	@echo "Done, cache is ready for use"