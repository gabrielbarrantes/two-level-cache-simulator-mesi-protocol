/*
 * MESIBlock.h
 *
 *  Created on: Nov 8, 2018
 *      Author: gab
 */

#ifndef MESIBLOCK_HPP_
#define MESIBLOCK_HPP_

//#include "cacheBlockMESI.h"

class MESIBlock
{
    public:
        MESIBlock(cacheBlockMESI *i_pCacheBlockP1L1, cacheBlockMESI *i_pCacheBlockP2L1, cacheBlockMESI *i_pCacheBlockCommonL2);
        ~MESIBlock();
        
        cacheBlockMESI *m_pCacheBlockP1L1;
        cacheBlockMESI *m_pCacheBlockP2L1;
        cacheBlockMESI *m_pCacheBlockCommonL2;
        
        void DataAccess(long i_liAddress, int i_iLoadStore);

    protected:
    private:
        int m_iMESIStateP1L1;
        int m_iMESIStateP2L1;
};

#endif /* MESIBLOCK_HPP_ */
