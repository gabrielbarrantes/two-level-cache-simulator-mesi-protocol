/*
 * cacheLRU.h
 *
 *  Created on: Nov 8, 2018
 *      Author: gab
 */

#ifndef CACHELRU_HPP_
#define CACHELRU_HPP_

#include "cacheBlockLRUMESI.h"
#include "cacheDirectMap.h" 

class CacheLRU
{
    public:
        CacheLRU(int sizeKB, int blockSizeBytes, int associativity, int i_iNumberOfCache, CacheDirectMap * i_pCacheCommonL2);
        ~CacheLRU();
        
        void DataAccess(long i_liAddress, int i_iLoadStore);
        void CalculateStatistics();
        
        int m_iSize;
        int m_iAssociativity;
        int m_iBlockSizeBytes;
        
        double       m_dOverallMissrate;
        double       m_dReadMissRate;
        unsigned int m_uiDirtyEvictions;
        unsigned int m_uiLoadMisses;
        unsigned int m_uiStoreMisses;
        unsigned int m_uiTotalMisses;
        unsigned int m_uiLoadHits;
        unsigned int m_uiStoreHits;
        unsigned int m_uiTotalHits;
        cacheBlockLRUMESI **m_pCache;
        
    protected:
    private:
        int m_iNumberOfCache; 
        int m_iNumberOfOffsetBits;
        int m_iNumberOfIndexBits;
        int m_iNumberOfTagBits;
        int m_iNumberOfSets;
        int m_iNumberOfVias;
        unsigned int m_uiIndexMask;
        unsigned int m_uiTagMask;
        //cacheBlockLRUMESI **m_pCache;
        CacheDirectMap * m_pCacheCommonL2;
};

#endif /* CACHELRU_HPP_ */
