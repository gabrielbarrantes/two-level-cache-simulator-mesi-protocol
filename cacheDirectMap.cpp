/*
 * cacheDirectMap.cpp
 *
 *  Created on: Nov 8, 2018
 *      Author: gab
 */

#include <math.h>
#include "cacheDirectMap.h"
#include "cacheBlockLRUMESI.h"

#define wordSizeBytes 1
#define addressSizeBits 32

CacheDirectMap::CacheDirectMap(int i_iSizeKB, int i_iBlockSizeBytes)
{
    m_iSize = i_iSizeKB;
    m_iAssociativity = 1;
    m_iBlockSizeBytes = i_iBlockSizeBytes;
    
    int l_iCacheSizeBytes = (int) pow (2, (int) round( log((double) (i_iSizeKB*1000))/log(2) ));
    
    m_iNumberOfSets = l_iCacheSizeBytes/i_iBlockSizeBytes;
    m_iNumberOfVias = 1;
    
    m_iNumberOfIndexBits  = (int) (log(m_iNumberOfSets)/log(2));
    m_iNumberOfOffsetBits = (int) (log(i_iBlockSizeBytes/wordSizeBytes)/log(2));
    m_iNumberOfTagBits    = addressSizeBits - m_iNumberOfIndexBits - m_iNumberOfOffsetBits;
    
    //Generate masks
    m_uiIndexMask = 0;
    for(int i=0; i<m_iNumberOfIndexBits;i++){m_uiIndexMask = (m_uiIndexMask<<1)+1;}
    
    m_uiTagMask = 0;
    for(int i=0; i<m_iNumberOfTagBits;i++){m_uiTagMask = (m_uiTagMask<<1)+1;}

    //Set initial values
    m_dOverallMissrate  = 0.0;
    m_dReadMissRate     = 0.0;
    m_uiDirtyEvictions  = 0;
    m_uiLoadMisses      = 0;
    m_uiStoreMisses     = 0;
    m_uiTotalMisses     = 0;
    m_uiLoadHits        = 0;
    m_uiStoreHits       = 0;
    m_uiTotalHits       = 0;
    m_uiTotalHits       = 0;
    
    m_iNumberOfAccesses = 0;
    m_iCoherenceInvalidationsP0 = 0;
    m_iCoherenceInvalidationsP1 = 0;
    
    //Allocate memory for cache's blocks
    m_pCache = (cacheBlockMESI *) malloc (4096*sizeof(cacheBlockMESI ));
    //if (cache  == NULL){ return 1; }
    //Set the initial values of cache
    for(int i=0;i<4096;i++)
    {
        m_pCache[i].m_iValidBit  = 0; 
        m_pCache[i].m_iDirtyBit  = 0;
        //m_pCache[i].m_iLRUIndex  = 0; //no hace falta
        //m_pCache[i].m_iMESIState = 0;
        m_pCache[i].m_iMESIStateL1P0 = 0; // 0 = invalid
        m_pCache[i].m_iMESIStateL1P1 = 0; // 0 = invalid
    }
    
}

void CacheDirectMap::DataAccess(long i_liAddress, int i_iLoadStore, int i_iCacheNumber, cacheBlockLRUMESI * i_pCacheBlockLRUMESIL1, int i_iHitL1)
{
    unsigned int index = (i_liAddress>> m_iNumberOfOffsetBits) & m_uiIndexMask;
    unsigned int tag   = (i_liAddress>> (m_iNumberOfIndexBits+m_iNumberOfOffsetBits) ) & m_uiTagMask;
    
    m_iNumberOfAccesses++;
    int hit =0;
    
    //Look for Hit
    if((m_pCache[index].m_uiTag == tag) && (m_pCache[index].m_iValidBit==1))
    {
        hit = 1;
        if(i_iHitL1 == 0)
        {
            if(i_iLoadStore){ m_uiStoreHits++;}
            else { m_uiLoadHits++; }
        }
    }

    //if not hit, look envit candidate and replace the block in cache
    else
    {       
        //if(m_pCache[index].m_iValidBit==1 && m_pCache[index].m_iDirtyBit==1){m_uiDirtyEvictions++;}
        
        m_pCache[index].m_uiTag = tag;
        m_pCache[index].m_iValidBit = 1;
        if(i_iLoadStore)
        {
            m_uiStoreMisses ++; 
            //m_pCache[index].m_iDirtyBit =1;
        } else {
            m_uiLoadMisses ++;
            //m_pCache[index].m_iDirtyBit = 0; 
        }
    }
    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////
    if(i_iHitL1==0)
    {
        int *ptr;
        ptr = i_pCacheBlockLRUMESIL1->m_iMESIState;
        if(ptr != NULL){*ptr = 0;}
    }
    
    if(i_iCacheNumber) // acceso de la cache 1
    {
        i_pCacheBlockLRUMESIL1->m_iMESIState = & m_pCache[index].m_iMESIStateL1P1;
    }else //acceso de la cache 0
    {
        i_pCacheBlockLRUMESIL1->m_iMESIState = & m_pCache[index].m_iMESIStateL1P0;
    }
    
    if(hit)  //si era hit de L2
    {
        if(i_iLoadStore) //un store
        {
            if(i_iCacheNumber)
            {
                m_pCache[index].m_iMESIStateL1P1 = 3;
                if(m_pCache[index].m_iMESIStateL1P0 != 0)
                {
                    m_pCache[index].m_iMESIStateL1P0 = 0; //sumar un invalid
                    m_iCoherenceInvalidationsP0++;
                }
            }
            else
            {
                m_pCache[index].m_iMESIStateL1P0 = 3;
                if(m_pCache[index].m_iMESIStateL1P1 != 0)
                {
                    m_iCoherenceInvalidationsP1++;
                    m_pCache[index].m_iMESIStateL1P1 = 0; //sumar un invalidCoherence
                }
            }
        }
        else  //un load
        {
            if(i_iCacheNumber) //load de cache1
            {
                if(m_pCache[index].m_iMESIStateL1P0 == 0){m_pCache[index].m_iMESIStateL1P1 =1;}
                else { m_pCache[index].m_iMESIStateL1P1 =2; m_pCache[index].m_iMESIStateL1P0 =2;}
            }
            else //load de cache 0
            {
                if(m_pCache[index].m_iMESIStateL1P1 == 0){m_pCache[index].m_iMESIStateL1P0 =1;}
                else { m_pCache[index].m_iMESIStateL1P0 =2; m_pCache[index].m_iMESIStateL1P1 =2;}
            }
            
        }
    }
    
    else    //si era miss de L2
    {
        if(i_iCacheNumber) // acceso de la cache 1
        {
            if(i_iLoadStore) // si era store
            {
                m_pCache[index].m_iMESIStateL1P1 = 3;
                if(m_pCache[index].m_iMESIStateL1P0 != 0)
                {
                    m_pCache[index].m_iMESIStateL1P0 = 0; //sumar un invalid
                    m_iCoherenceInvalidationsP0++;
                }
            } else {
                m_pCache[index].m_iMESIStateL1P1 = 1;
                if(m_pCache[index].m_iMESIStateL1P0 != 0)
                {
                    m_pCache[index].m_iMESIStateL1P0 = 0; //sumar un invalid
                    m_iCoherenceInvalidationsP0++;
                }
            }
        }
                
        else //acceso de la cache 0
        {
            if(i_iLoadStore) // si era store
            {
                m_pCache[index].m_iMESIStateL1P0 = 3;
                if(m_pCache[index].m_iMESIStateL1P1 != 0)
                {
                    m_iCoherenceInvalidationsP1++;
                    m_pCache[index].m_iMESIStateL1P1 = 0; //sumar un invalidCoherence
                }
            } else {
                m_pCache[index].m_iMESIStateL1P0 = 1;
                if(m_pCache[index].m_iMESIStateL1P1 != 0)
                {
                    m_iCoherenceInvalidationsP1++;
                    m_pCache[index].m_iMESIStateL1P1 = 0; //sumar un invalidCoherence
                }
            }
        }
    }
    /*
    if(i_iCacheNumber) // acceso de la cache 1
    {
        i_pCacheBlockLRUMESIL1 = &m_pCache[index].m_iMESIStateL1P1;
        i_pCacheBlockLRUMESIL1->m_iValidBit = m_pCache[index].m_iMESIStateL1P1;
    }else //acceso de la cache 0
    {
        i_pCacheBlockLRUMESIL1 = &m_pCache[index].m_iMESIStateL1P0;
        i_pCacheBlockLRUMESIL1->m_iValidBit = m_pCache[index].m_iMESIStateL1P0;
    }
    */
    
}

void CacheDirectMap::CalculateStatistics()
{
    m_uiTotalMisses=m_uiStoreMisses+m_uiLoadMisses;
    m_uiTotalHits=m_uiStoreHits+m_uiLoadHits;
    m_dReadMissRate= ((double ) (m_uiLoadMisses) )/ ((double) (m_uiLoadMisses+m_uiLoadHits) );
    m_dOverallMissrate= ((double) (m_uiTotalMisses) )/((double) (m_uiTotalMisses+m_uiTotalHits));
    m_dGlobalMissrate = ((double) (m_uiTotalMisses) )/((double) (m_iNumberOfAccesses));
}

CacheDirectMap::~CacheDirectMap()
{
    //Allocate memory for cache's blocks
    //if (cache  == NULL){ return 1; }
    //for (int i=0;i<m_iNumberOfSets;i++){ free(m_pCache[i]); m_pCache[i] = NULL; }
    //free(m_pCache); m_pCache=NULL;
}
