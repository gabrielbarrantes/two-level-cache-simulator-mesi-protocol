#ifndef CACHEBLOCKLRUMESI_H_
#define CACHEBLOCKLRUMESI_H_

//Struct for representing a cache block for LRU $ Level 1

typedef struct 
{
    //int m_iValidBit;
    //int m_iDirtyBit;
    unsigned int m_uiTag;
    int m_iLRUIndex;
    int * m_iMESIState; //pointer to the MESI State stored in $L2
} cacheBlockLRUMESI ; 

#endif /* CACHEBLOCKLRUMESI_H_ */
